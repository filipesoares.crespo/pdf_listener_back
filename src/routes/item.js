const router = require("express").Router();
const ItemController = require("../app/controller/ItemController");

const multer = require("multer");
const multerConfig = require("../config/multer");

router.get("/", ItemController.index );
router.post("/", multer(multerConfig).single("file"), ItemController.store );
router.delete("/:id", ItemController.delete );
router.get("/:id", ItemController.show );
router.get("/stream/:id", ItemController.stream);

module.exports = router;
