const router = require("express").Router();
const UserController = require("../app/controller/UserController");

router.post("/login", UserController.login);
router.post("/logout", UserController.logout);
router.post("/singup", UserController.store);

module.exports = router;
