const router = require("express").Router();
const cors = require('cors');

router.use(cors());

const authMiddleware = require("./app/middleware/auth");
// const routeNotFound = require("./app/middleware/routeNotFound");

router.get("/", (req, res) => res.json({ api: "active" }));

router.use("/users", require("./routes/user"));

router.use(authMiddleware);

router.use("/itens", require("./routes/item"));

router.get("/dashboard", (req, res) => {
  return res.status(200).send();
});

// router.use(routeNotFound);

module.exports = router;
