const { Item } = require('../models')

class ItemController {
  async store(req, res) {
    let item = await Item.create({
      name: req.file.originalname,
      key: req.file.filename,
      url: req.file.path,
      size: req.file.size,
      duration: 0,
      favorite: false,
      completed: false,
      user_id: req.userId
    })

    return res.status(200).send({item})
  }

  async delete(req, res) {
    const { id } = req.params;

    await Item.destroy({ where:{ id, user_id: req.userId }})
    .then(error => {
      if (error){
        return res.status(200).send({message: "Deleted if success"})
      }
      return res.status(401).send({message: "Item not exist"})
    })
  }

  async show(req, res) {
    const { id } = req.params;
    let item = await Item.findOne({ where:{ id, user_id: req.userId }})
    if (item){
      res.json(item)
    } else {
      res.status(402);
      res.json({message: "Item not exist"})
    }
  }

  async index(req, res) {
    let itens = await Item.findAll({ where: { user_id: req.userId }})
    res.json(itens)
  }

  async stream(req, res){
    try {
      var trackID = new ObjectID(req.params.trackID);
    } catch(err) {
      return res.status(400).json({ message: "Invalid trackID in URL parameter. Must be a single String of 12 bytes or a string of 24 hex characters" });
    }
    res.set('content-type', 'audio/mp3');
    res.set('accept-ranges', 'bytes');

    let bucket = new mongodb.GridFSBucket(db, {
      bucketName: 'tracks'
    });

    let downloadStream = bucket.openDownloadStream(trackID);

    downloadStream.on('data', (chunk) => {
      res.write(chunk);
    });

    downloadStream.on('error', () => {
      res.sendStatus(404);
    });

    downloadStream.on('end', () => {
      res.end();
    });
  }
}

module.exports = new ItemController()
