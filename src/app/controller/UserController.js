const { User } = require("../models");

class UserController {
  async store(req, res) {
    const { name, email, password } = req.body;
    let user = await User.findOne({ where: { email } });

    if (user) {
      return res.status(401).json({ message: "Email already used" });
    }

    user = await User.create({
      name,
      email,
      password
    });

    return res.status(200).send({
      user,
      token: user.generateToken()
    });
  }

  async login(req, res) {
    const { email, password } = req.body;

    const user = await User.findOne({ where: { email } });
    if (!user) {
      return res.status(401).json({ message: "User not found" });
    }

    if (!(await user.checkPassword(password))) {
      return res.status(401).json({ message: "Incorrect password " });
    }

    return res.status(200).send({
      user,
      token: user.generateToken()
    });
  }

  async logout(req, res) {
    const { email } = req.body;

    return res.status(200).send({ message: `logged out user ${email}` });
  }
}

module.exports = new UserController();
