FROM node:latest

WORKDIR /app

ADD package.json /app/

RUN npm install -g npm
RUN npm install -g supervisor
RUN npm install -g jshint
RUN npm install -g jest
RUN npm install -g sequelize-cli
RUN npm install -g nodemon
# RUN npm install nodemon --save-dev
RUN npm cache clean --force
RUN npm install
