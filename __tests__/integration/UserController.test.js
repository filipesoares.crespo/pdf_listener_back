const request = require("supertest")

const app = require("../../src/app")
const truncate = require("../utils/truncate")
const factory = require("../factories")

describe("Login", () => {
  afterEach(async () => {
    await truncate()
  })

it("should authenticate with valid credentials", async () => {
    let user = await factory.create("User")

    let response = await request(app)
    .post("/users/login")
    .send({
      email: user.email,
      password: '123456'
    })

    expect(response.status).toBe(200)
  })

  it("should not authenticate with invalid credentials", async () => {
    let user = await factory.create("User", {
      password: "1234567"
    })

    let response = await request(app)
      .post("/users/login")
      .send({
        email: user.email,
        password: "123456"
      })

    expect(response.status).toBe(401)
  })

  it("should return jwt token when authenticated", async () => {
    let user = await factory.create("User")

    let response = await request(app)
      .post("/users/login")
      .send({
        email: user.email,
        password: user.password
      })

    expect(response.body).toHaveProperty("token")
  })

  it("should return user not found", async () => {
    let user = await factory.create("User")
    let expectedReturn = {"message": "User not found"}

    let response = await request(app)
      .post("/users/login")
      .send({
        email: "f@f.com.br",
        password: user.password
      })

    expect(response.body).toStrictEqual(expectedReturn)
    expect(response.body).not.toHaveProperty("token")
  })

  it("should be able to access private routes when authenticated", async () => {
    let user = await factory.create("User", {
      password: "123456"
    })

    let response = await request(app)
      .get("/dashboard")
      .set("Authorization", `Bearer ${user.generateToken()}`)

    expect(response.status).toBe(200)
  })

it("should be able to access private routes without jwt token", async () => {
    let response = await request(app).get("/dashboard")

    expect(response.status).toBe(401)
  })

it("should be able to access private routes with invalid jwt token", async () => {
    let response = await request(app)
      .get("/dashboard")
      .set("Authorization", "Bearer 1234567890123456789")

    expect(response.status).toBe(401)
  })
})

describe("Store", () => {
  beforeEach(async () => {
    await truncate()
  })

it("should create new user and return valid token", async () => {
    let response = await request(app)
      .post("/users/singup")
      .send({
        name: 'filipe',
        email: 'f@f.com',
        password: "123456"
      })

    expect(response.status).toBe(200)
    expect(response.body).toHaveProperty("token")
  })

it("should retornar erro if create new user with email in use", async () => {
    let expectedReturn = {"message": "Email already used"}
    let responseOne = await request(app)
    .post("/users/singup")
    .send({
      name: 'filipe',
      email: 'f@f.com',
      password: "123456"
    })

    let response = await request(app)
      .post("/users/singup")
      .send({
        name: 'filipe',
        email: 'f@f.com',
        password: "123456"
      })

    expect(responseOne.status).toBe(200)
    expect(response.status).toBe(401)
    expect(response.body).toStrictEqual(expectedReturn)
  })
})

describe("Logout", () => {
  beforeEach(async () => {
    await truncate()
  })

  it("should logout user", async () => {
    let response = await request(app)
      .post("/users/logout")
      .send({
        email: 'f@f.com'
      })

    expect(response.status).toBe(200)
    expect(response.body).not.toHaveProperty("token")
  })
})