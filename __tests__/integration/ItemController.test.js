const request = require('supertest')

const app = require('../../src/app')
const truncate = require("../utils/truncate")
const factory = require("../factories")

describe("Item Controller", () => {
  let expectedFileName = 'tdd.pdf'
  let token = ''
  let user = ''

  beforeEach(async () => {
    await truncate()
    user = await factory.create('User')
    token = `Bearer ${user.generateToken()}`
  })

  describe("store new item", () => {
    it("should authenticate with valid credentials", async () => {
      let response = await request(app)
        .post("/itens")
        .attach('file', '__tests__/tmp/pinguim.jpg')
        .set("Authorization", token)

      expect(response.status).toBe(500)
    })

    it("should authenticate with valid credentials", async () => {
      let expectedFileName = 'tdd.pdf'
      let response = await request(app)
        .post("/itens")
        .attach('file', `__tests__/tmp/${expectedFileName}`)
        .set("Authorization", token)

      expect(response.status).toBe(200)
      expect(response.body.item.name).toBe(expectedFileName)
      expect(response.body.item.user_id).toBe(user.id)
    })
  })

  describe("delete item", () => {
    it("should remove item", async () => {
      let expectedResponseBody = {"message": "Deleted if success"}
      let response = await request(app)
        .post("/itens")
        .attach('file', `__tests__/tmp/${expectedFileName}`)
        .set("Authorization", token)

      let item_id = response.body.item.id

      let responseRemove = await request(app)
        .delete(`/itens/${item_id}/`)
        .set("Authorization", token)

        expect(responseRemove.status).toBe(200)
        expect(responseRemove.body).toStrictEqual(expectedResponseBody)
    })

    it("should return erro if item donst exist", async () => {
      let expectedResponseBody = {"message": "Item not exist"}
      let item_id = 10000

        let responseRemove = await request(app)
        .delete(`/itens/${item_id}`)
        .set("Authorization", token)

      expect(responseRemove.status).toBe(401)
      expect(responseRemove.body).toStrictEqual(expectedResponseBody)
    })

  })

  describe("show item", () => {
    it("should return item", async () => {
      let response = await request(app)
        .post("/itens")
        .attach('file', `__tests__/tmp/${expectedFileName}`)
        .set("Authorization", token)

      let item = response.body.item

      let responseShow = await request(app)
      .get(`/itens/${item.id}/`)
      .set("Authorization", token)

      expect(responseShow.status).toBe(200)
      expect(responseShow.body).toEqual(item)
    })

    it("should return item", async () => {
      let expectedResponseBody = {"message": "Item not exist"}

      let response = await request(app)
        .post("/itens")
        .attach('file', `__tests__/tmp/${expectedFileName}`)
        .set("Authorization", token)

      let item = response.body.item

      let responseShow = await request(app)
      .get(`/itens/${item.id + 1}/`)
      .set("Authorization", token)

      expect(responseShow.status).toBe(402)
      expect(responseShow.body).toStrictEqual(expectedResponseBody)
    })
  })
})
